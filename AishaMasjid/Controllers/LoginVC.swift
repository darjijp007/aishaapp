//
//  LoginVC.swift
//  NextGen
//  Created by SBC on 10/09/18.
//  Copyright © 2018 SBC. All rights reserved.

import UIKit
import Photos

class LoginVC: UIViewController, Storyboardable {
    
    static let storyboardName: storyBoardName = .Login
    
    // MARK: Proprties
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Action Functions
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        NavigationManager.shared.setFrontViewController(SignUpVC.storyboardViewController())
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        NavigationManager.shared.setFrontViewController(HomeVC.storyboardViewController())
    }
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        self.navigationController?.pushViewController(ForgotPasswordVC.storyboardViewController(), animated: true)
    }
}

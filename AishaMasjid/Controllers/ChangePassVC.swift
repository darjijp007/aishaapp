//
//  ChangePassVC.swift
//  AishaMasjid
//
//  Created by Apple on 08/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ChangePassVC: BaseVC, Storyboardable {
    static var storyboardName: storyBoardName = .Login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = "Change Password"
    }
}

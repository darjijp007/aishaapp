//
//  ReminderVC.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ReminderVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .Education
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func configureUI() {
        super.configureUI()
        setBackButton()
        headerView.titleLabel.text = "Reminders"
        tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        Helper.shared.hideFooterSpace(tableView: tableView)
    }
}

extension ReminderVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.titleLabel.text = "Reminder \(indexPath.row + 1)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = TableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
        view.contentView.backgroundColor = themeColor
        if section == 0 {
            view.headerTitleButton.setTitle("Today", for: .normal)
        } else {
            view.headerTitleButton.setTitle("Tomorrow", for: .normal)
        }
        return view
    }
}

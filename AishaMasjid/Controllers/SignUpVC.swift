//
//  SignUpVC.swift
//  NextGen
//
//  Created by SBC on 13/09/18.
//  Copyright © 2018 SBC. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UITextFieldDelegate, Storyboardable{
    
    static let storyboardName: storyBoardName = .Login
    
    // MARK: Properties
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet var btncheck: UIButton!

    //Variables
    let datePicker = UIDatePicker()
    var strSurgeryId = String()
    var strDate = String()
    
 var facebookProfileUrl: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Action Functions
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        NavigationManager.shared.setFrontViewController(HomeVC.storyboardViewController())
    }
    //Action Functions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        NavigationManager.shared.setFrontViewController(LoginVC.storyboardViewController())
    }
}

//
//  ChildrenCell.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ChildrenCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    func configure(_ child: Children) {
        nameLabel.text = child.name
        ageLabel.text = child.age
    }
}

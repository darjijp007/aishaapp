//
//  ChildrenListVC.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ChildrenListVC: BaseVC, Storyboardable {
    
    static let storyboardName: storyBoardName = .Education
    var childrens: [Children] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = "Education"
        Helper.shared.noDataAvailabe(tableView: tableView, titleText: "You have not added your childrens yet", hide: false)
    }
    
    @IBAction func addChildrenButtonTapped(_ sender: Any) {
        let childView = UtilityManager.shared.showAddChildView()
        childView.delegate = self
        self.view.addSubview(childView)
    }
}

extension ChildrenListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childrens.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenCell") as! ChildrenCell
        cell.configure(childrens[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = EducationOptionsVC.storyboardViewController()
        vc.child = childrens[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension ChildrenListVC: AddChildDelegate {
    func childAdded(_ childCode: String) {
        let child = Children(name: "Yash", code: "001", age: "3 yr")
        childrens.append(child)
        tableView.reloadData()
        Helper.shared.hideFooterSpace(tableView: tableView)
    }
}

//
//  AttendanceVC.swift
//  AishaMasjid
//
//  Created by Apple on 15/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class AttendanceVC: BaseVC, Storyboardable {
    static var storyboardName: storyBoardName = .Education
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        setBackButton()
        headerView.titleLabel.text = "Attendance"
    }
}

extension AttendanceVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceCell") as! AttendanceCell
        if indexPath.row == 0 {
            cell.titleLabel.text = "Total Present"
            cell.presentLabel.text = "27"
        } else {
            cell.titleLabel.text = "Total Absent"
            cell.presentLabel.text = "3"
        }
        return cell
    }
}

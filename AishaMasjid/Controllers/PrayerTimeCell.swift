//
//  PrayerTimeCell.swift
//  AishaMasjid
//
//  Created by Apple on 07/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class PrayerTimeCell: UITableViewCell {
    @IBOutlet weak var prayerLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var alarmImageView: UIImageView!
    
    func configure(_ prayerItem: Prayer) {
        prayerLabel.text = prayerItem.prayerName
        timeLabel.text  = prayerItem.time
        alarmImageView.image = UIImage(named: prayerItem.imageName!)
    }
    
}

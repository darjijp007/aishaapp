//
//  SideVc.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//


import Foundation
import UIKit

class SideVC: UIViewController, Storyboardable {
    
    //Mark: Vars
    static let storyboardName: storyBoardName = .SideView
    var menuItems: [MenuItem] = []
    
    //Mark: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //configure UI
    func configureUI() {
        menuItems = [MenuItem(title: .Home, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .NewsAndEvents, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .Donation, imageName: "ic_profile_placeholder"),
                     MenuItem(title: .Education, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .ContactUS, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .Profile, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .ChangePassword, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .AboutUs, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .Privacy, imageName: "ic_profile_placeholder"),
                    MenuItem(title: .Logout
                        , imageName: "ic_profile_placeholder"),]
        
        tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        Helper.shared.hideFooterSpace(tableView: tableView)
        tableView.backgroundView = nil
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
    }
    
    //push selected selected viewcontroller
    func pushToRevealViewController(_ controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
        let revealViewController: SWRevealViewController = self.revealViewController()
        revealViewController.pushFrontViewController(navigationController, animated: true)
    }
    
    //on select left menu
    func selectLeftMenu(_ selectedMenu: sideMenuItems) {
        switch selectedMenu {
        case .Home: do {
            NavigationManager.shared.setFrontViewController(HomeVC.storyboardViewController())
            break
            }
        case .NewsAndEvents:  do {
            NavigationManager.shared.setFrontViewController(EventsVC.storyboardViewController())
            break
            }
        case .Donation: do {
            NavigationManager.shared.setFrontViewController(DonationVC.storyboardViewController())
            break
            }
        case .AzanAlarm: do {
            NavigationManager.shared.setFrontViewController(AzanAlarmsVC.storyboardViewController())
            break
            }
        case .ContactUS: do {
            NavigationManager.shared.setFrontViewController(ContactUsVC.storyboardViewController())
            break
            }
        case .Profile: do {
            NavigationManager.shared.setFrontViewController(ProfileVC.storyboardViewController())

            break
            }
            
        case .ChangePassword: do {
            NavigationManager.shared.setFrontViewController(ChangePassVC.storyboardViewController())

            break
            }
        case .AboutUs: do {
           let webVC =  WebViewVC.storyboardViewController()
            webVC.titleString = sideMenuItems.AboutUs.rawValue
            NavigationManager.shared.setFrontViewController(webVC)

            break
            }
        case .Privacy: do {
            let webVC =  WebViewVC.storyboardViewController()
            webVC.titleString = sideMenuItems.Privacy.rawValue
            NavigationManager.shared.setFrontViewController(webVC)
            break
            }
        case .Logout: do {
            NavigationManager.shared.setFrontViewController(LoginVC.storyboardViewController())
            break
            }
        case .Education: do {
            NavigationManager.shared.setFrontViewController(ChildrenListVC.storyboardViewController())
            }
        }
    }
    
    func askToLogout() {
        let logoutAlert = UIAlertController(title: "Alert!", message: "Are you sure want to logout?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] action in
            self?.callLogoutApi()
        }
        let noAction = UIAlertAction(title: "No", style: .destructive) { action in
        }
        logoutAlert.addAction(noAction)
        logoutAlert.addAction(yesAction)
        self.present(logoutAlert, animated: true, completion: nil)
    }
    
    func callLogoutApi() {
        showHud()
    }
    
}

extension SideVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.configure(menuItems[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedMenu = menuItems[indexPath.row]
        selectLeftMenu(selectedMenu.title!)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundView = nil
        cell.backgroundColor = .clear
    }
}


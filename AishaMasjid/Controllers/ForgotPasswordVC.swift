//
//  ForgotPasswordVC.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/5/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordVC: BaseVC, Storyboardable {
    
    static let storyboardName: storyBoardName = .Login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        self.setBackButton()
    }
}

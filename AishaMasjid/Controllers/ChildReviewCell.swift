//
//  ChildReviewCell.swift
//  AishaMasjid
//
//  Created by Apple on 16/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ChildReviewCell: UITableViewCell {
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}

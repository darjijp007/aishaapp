//
//  AzanAlarmsVC.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class AzanAlarmsVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .Home
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
    }
}

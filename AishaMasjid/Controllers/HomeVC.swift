//
//  HomeVC.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//
struct Prayer {
    var prayerName: String?
    var time: String?
    var imageName: String?
}
import Foundation
import UIKit

class HomeVC: BaseVC, Storyboardable {
    
    static let storyboardName: storyBoardName = .Home
    var prayers: [Prayer] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = "Prayer Time"
        Helper.shared.hideFooterSpace(tableView: tableView)
        prayers = [Prayer(prayerName: "Fajr", time: "5:08 AM", imageName: "ic_volume"),
                   Prayer(prayerName: "Surrise", time: "6:24 AM", imageName: "ic_none"),
                   Prayer(prayerName: "Dhuhr", time: "12:38 PM", imageName: "ic_bell"),
                   Prayer(prayerName: "Asr", time: "4:05 PM", imageName: "ic_bell_silent"),
                   Prayer(prayerName: "Maghrib", time: "6:51 PM", imageName: "ic_volume"),
                   Prayer(prayerName: "isha'a", time: "8:07 PM", imageName: "ic_volume")]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = navigationColor
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prayers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrayerTimeCell") as! PrayerTimeCell
        let prayer = prayers[indexPath.row]
        cell.configure(prayer)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  70
    }
    
}

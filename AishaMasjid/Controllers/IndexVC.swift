//
//  IndexVC.swift
//  NextGen
//
//  Created by SBC on 13/09/18.
//  Copyright © 2018 SBC. All rights reserved.
//

import UIKit

class IndexVC: UIViewController, Storyboardable {
    static var storyboardName: storyBoardName = .Login
    
    
    // MARK: Properties
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Action Function
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        LoginManager.shared.setUpLoginViewController(LoginVC.storyboardViewController())
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
    LoginManager.shared.setUpLoginViewController(SignUpVC.storyboardViewController())
    }
    
}

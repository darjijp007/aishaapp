//
//  EducationOptionsVC.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class EducationOptionsVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .Education
    var child: Children?
    var options: [EducationOption] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func configureUI() {
        super.configureUI()
        setBackButton()
        headerView.titleLabel.text = child?.name
        Helper.shared.hideFooterSpace(tableView: tableView)
        options = [EducationOption(title: .Reminders, imageName: "ic_profile_placeholder"),
                  EducationOption(title: .Notifications, imageName: "ic_profile_placeholder"),
                  EducationOption(title: .Assignments, imageName: "ic_profile_placeholder"),
                  EducationOption(title: .Review, imageName: "ic_profile_placeholder"),
                  EducationOption(title: .Attendance, imageName: "ic_profile_placeholder")]
        tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
    }
}
extension EducationOptionsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.titleLabel.text = options[indexPath.row].title?.rawValue
        cell.iconImageView.image = UIImage(named: options[indexPath.row].imageName!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let reminderVC = ReminderVC.storyboardViewController()
            self.navigationController?.pushViewController(reminderVC, animated: true)
        } else if indexPath.row == 1 {
            let notificationVC = NotificationsVC.storyboardViewController()
            self.navigationController?.pushViewController(notificationVC, animated: true)

        } else if indexPath.row == 2 {
            let assignmentVC = AssignmentListVC.storyboardViewController()
            self.navigationController?.pushViewController(assignmentVC, animated: true)

        } else if  indexPath.row == 3 {
            let attendanceVC = ChildReviewVC.storyboardViewController()
            self.navigationController?.pushViewController(attendanceVC, animated: true)
            
        } else  {
            let attendanceVC = AttendanceVC.storyboardViewController()
            self.navigationController?.pushViewController(attendanceVC, animated: true)

        }
    }
}

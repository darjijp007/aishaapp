//
//  AboutVC.swift
//  AishaMasjid
//
//  Created by Apple on 07/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebViewVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .SideView
    var titleString: String?
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = titleString
        loadWebView()
    }
    func loadWebView() {
        if titleString == sideMenuItems.AboutUs.rawValue {
            let myURL = URL(string:webUrls.aboutUs)
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        } else {
            let myURL = URL(string:webUrls.privacy)
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        }
    }
}

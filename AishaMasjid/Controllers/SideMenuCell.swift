//
//  SideMenuCell.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/8/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func configure(_ menuItem: MenuItem) {
        iconImageView.image = UIImage(named: menuItem.imageName!)
        titleLabel.text = menuItem.title?.rawValue
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = .clear
        self.backgroundView = nil
    }
}

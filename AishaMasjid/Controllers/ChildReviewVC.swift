//
//  ChildReviewVC.swift
//  AishaMasjid
//
//  Created by Apple on 16/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class ChildReviewVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .Education
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = "Review"
        setBackButton()
        tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
    }
}
extension ChildReviewVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildReviewCell") as! ChildReviewCell
        if indexPath.row == 0 {
            cell.subjectLabel.text = "Maths"
        } else {
            cell.subjectLabel.text = "Science"
        }
        return cell
    }
}

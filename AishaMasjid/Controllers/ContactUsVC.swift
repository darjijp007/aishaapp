//
//  ContactUsVC.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown

class ContactUsVC: BaseVC, Storyboardable {
    
    static let storyboardName: storyBoardName = .Home
    
    @IBOutlet weak var subjectDropDown: DropDown!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func configureUI() {
        super.configureUI()
        headerView.titleLabel.text = "Contact Us"
        subjectDropDown.optionArray = ["Funeral Request Form", "Nikah Related","Maktab (1st Sessino)","Maktab (2nd Session)","Maktab (Weekends)", "Other"]
        subjectDropDown.isSearchEnable = false
        subjectDropDown.selectedRowColor = self.view.backgroundColor!
    }
    
    //Action functions
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        
    }
}

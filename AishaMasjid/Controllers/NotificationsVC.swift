//
//  NotificationsVC.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class NotificationsVC: BaseVC, Storyboardable {
    static let storyboardName: storyBoardName = .Education
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func configureUI() {
        super.configureUI()
        setBackButton()
        headerView.titleLabel.text = "Notifications"
        Helper.shared.hideFooterSpace(tableView: tableView)
    }
}

extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        return cell
    }
}

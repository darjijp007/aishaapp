//
//  AppEnumConst.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation

enum sideMenuItems: String {
    case Home = "Prayer Time"
    case NewsAndEvents = "News & Events"
    case Donation
    case AzanAlarm = "Azan Alarm"
    case ContactUS = "Contact Us"
    case Profile
    case ChangePassword = "Change Password"
    case Logout
    case Privacy = "Privacy Policy"
    case AboutUs = "About us"
    case Education = "Education"
}

enum EducationOptions: String {
    case Reminders
    case Notifications
    case Assignments
    case Review
    case Attendance 
}

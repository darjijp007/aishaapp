//
//  AppConstants.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let navigationColor = UIColor(red: 68.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0)
let themeColor = UIColor(red: 255.0/255.0, green: 210.0/255.0, blue: 0.0/255.0, alpha: 1.0)


func isIpad() -> Bool {
    if UIDevice.current.userInterfaceIdiom == .pad {
        return true
    }
    return false
}

func proportionalHeight(_ height: Int) -> Int{
    if isIpad() {
        let additionalHeight  = Double(height) * 0.4
        return Int(Double(height) +  additionalHeight)
    }
    return height
}

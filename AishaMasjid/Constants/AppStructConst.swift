//
//  AppStructConst.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation

//Side Menu Item
struct MenuItem {
    var title: sideMenuItems?
    var imageName: String?
}

struct EducationOption {
    var title: EducationOptions?
    var imageName: String?
}

//Utility
struct constantValues {
    
    struct general {
        static let locale = "en_US_POSIX"
        struct dateFormat {
            static let server = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            static let profile = "MMM, yyyy"
            static let ymdFormat = "yyyy-MM-dd"
        }
    }
}

struct errorMessages {
    static let kNoInternetConnectionMessage = "Please check your internet connection."
}

struct errorCode {
    static let noNetwork = 601
}
struct webUrls {
    static let aboutUs = "http://aishaislamiccentre.org.uk/about-us/"
    static let privacy = "http://aishaislamiccentre.org.uk/privacy-policy/"
}


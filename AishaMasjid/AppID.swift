//
//  AppID.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/2/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation

enum storyBoardName: String {
    case Login
    case Dashboard
    case SideView
    case Home
    case Education
}

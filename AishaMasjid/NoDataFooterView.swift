//
//  NoDataFooterView.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/20/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class NoDataFooterView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("NoDataFooterView", owner: self, options: nil)
        self.frame = contentView.frame
        self.addSubview(contentView)
    }
}

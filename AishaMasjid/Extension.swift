//
//  Extension.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/2/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import SVProgressHUD



extension SVProgressHUD {
    open class func show(_ status: String? = nil, maxTime: TimeInterval = 60) {
        status != nil ? SVProgressHUD.show(withStatus: status!) : SVProgressHUD.show()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + maxTime) {
            SVProgressHUD.dismiss()
        }
    }
}

extension UIView {
    
    func setBorder(width: CGFloat, color: UIColor, radius: CGFloat) {
        
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func dropShadow(cornerRadius: CGFloat = 0) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 3
        if cornerRadius != 0 {
            layer.cornerRadius = cornerRadius
        }
    }
}

extension NSObject {
    
    var isHudVisible: Bool {
        return SVProgressHUD.isVisible()
    }
    
    func showHud() {
        SVProgressHUD.show(maxTime: 60)
    }
    
    func dismissHud() {
        SVProgressHUD.dismiss()
    }
}

extension UIColor {
    
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
    
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    
    var toHexStringWithHash: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "#%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

// Add status bar
extension UIApplication {
     var statusBarView: UIView? {
           if #available(iOS 13.0, *) {
               let tag = 3848
               if let statusBar = self.keyWindow?.viewWithTag(tag) {
                   return statusBar
               } else {
                   let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                   statusBarView.tag = tag
                   statusBarView.backgroundColor = navigationColor
                   self.keyWindow?.addSubview(statusBarView)
                   return statusBarView
               }
           } else {
               if responds(to: Selector(("statusBar"))) {
                   return value(forKey: "statusBar") as? UIView
               }
           }
           return nil
       }
}
/*

@IBDesignable extension UIView {
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var roundRadius : Bool {
        set {
            self.layer.cornerRadius = self.frame.size.height / 2
            self.clipsToBounds = true
        }
        get {
            return layer.cornerRadius == (frame.size.height / 2)
        }
    }
}
*/

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.tintColorDidChange()
    }
}

extension UIWindow {
    static func topViewController() -> UIViewController? {
        var top = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
}

extension UITableViewCell {
    
    @objc func configureUI(_ model: Any, indexpath: IndexPath? = nil, viewController: UIViewController? = nil) {
    }
    @objc func cellDidSelect(_ tableView: UITableView? = nil, _ model: Any, _ indexpath: IndexPath? = nil) {
    }
    @objc func cellDidEndDisplay( _ model: Any?, indexPath: IndexPath) {
    }
    @objc func cellWillDisplay( _ model: Any, indexPath: IndexPath) {
    }
}

extension UICollectionViewCell {
    @objc func configureUI(_ model: Any, indexpath: IndexPath? = nil, viewController: UIViewController? = nil) {
    }
    @objc func cellDidSelect(_ collectionView: UICollectionView? = nil, _ model: Any, _ indexpath: IndexPath? = nil) {
    }
}

extension NSObject {
    @objc func getCellSize(isDynamic: Bool) -> CGSize {
        return CGSize.zero
    }
    @objc func getCellIdentifier(type: Int) -> String {
        return "Cell"
    }
}

extension Array {
    
    func getElement(_ index: Int) -> Any? {
        if index < self.count {
            return self[index]
        } else {
            return nil
        }
    }
}

extension Int {
    func secondsToHoursMinutesSeconds () -> (String, String, String) {
        let secondCalculation = (self % 3600) % 60
        let second = (secondCalculation < 10) ? "0\(secondCalculation)" : String(secondCalculation)
        let minuteCalculation = (self % 3600) / 60
        let minute = (minuteCalculation < 10) ? "0\(minuteCalculation)" : String(minuteCalculation)
        let hourCalculation = self / 3600
        let hour = (hourCalculation < 10) ? "0\(hourCalculation)" : String(hourCalculation)
        return (hour, minute, second)
    }
}

extension UIButton {
    
    func centerVertically(padding: CGFloat = 6.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
                return
        }
        
        let totalHeight = imageViewSize.height + titleLabelSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: 0,
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height/2),
            right: 0.0
        )
        
        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }
}

extension Date {
    var minimumDOB: Date {
        return Calendar.current.date(byAdding: .year, value: -100, to: self)!
    }
}

extension String {
    
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    var isValidContact: Bool {
        let phoneNumberRegex = "^[1-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func contains(find: String?) -> Bool{
        return self.range(of: find ?? "") != nil
    }
    
    func containsIgnoringCase(find: String?) -> Bool{
        return self.range(of: find ?? "", options: .caseInsensitive) != nil
    }
    
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
        return actualSize.height
    }
    
    func size(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size
    }
}

extension NSAttributedString {
    func height(withWidth width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], context: nil)
        return actualSize.height
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

private var AssociatedObjectHandle: UInt8 = 0

extension UIDatePicker {
    
    var agentDateFormate: DateFormatter {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as! DateFormatter
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

extension Date {
    
    func getString(formate: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = formate
        formatter.locale = Locale.init(identifier: constantValues.general.locale)
        return formatter.string(from: self)
    }
    
    static func getDateFromServerTime(time: String?) -> Date? {
        
        guard time != nil && !time!.isEmpty else {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = constantValues.general.dateFormat.server
        formatter.locale = Locale.init(identifier: constantValues.general.locale)
        return formatter.date(from:time!)
    }
}

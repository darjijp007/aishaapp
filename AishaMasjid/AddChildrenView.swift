//
//  AddChildrenView.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

protocol AddChildDelegate {
    func childAdded(_ childCode: String)
}
class AddChildrenView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    var delegate: AddChildDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("AddChildrenView", owner: self, options: nil)
        self.frame = contentView.frame
        self.addSubview(contentView)
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        delegate?.childAdded(codeTextField.text!)
        self.removeFromSuperview()
    }
}

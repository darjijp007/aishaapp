//
//  TableHeaderView.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/19/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//
protocol TableHeaderViewDelegate {
    func headerSelected()
}

import Foundation
import UIKit

class TableHeaderView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var headerTitleButton: UIButton!
    var delegate: TableHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("TableHeaderView", owner: self, options: nil)
        self.frame = contentView.frame
        self.addSubview(contentView)
        headerTitleButton.addTarget(self, action: #selector(headerTitleButtonTapped), for: .touchUpInside)
    }
    
    @objc func headerTitleButtonTapped(_ sender: UIButton) {
        delegate?.headerSelected()
    }
}

//
//  ViewController.swift
//  IWDMS
//
//  Created by Jaydip Darji  on 8/2/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    @IBOutlet weak var headerView: CustomHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //configure default ui
    func configureUI() {
        headerView.leftButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
    }
    
    //set Backbutton image and touch event
    func setBackButton() {
        headerView.leftButton.setImage(UIImage(named: "ic_back"), for: .normal)
        headerView.leftButton.removeTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        headerView.leftButton.addTarget(self, action: #selector(backButtonTapped)
            , for: .touchUpInside)
    }
    
    //back navigation
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}


//
//  NavigationManager.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/5/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class NavigationManager {
    
    static let shared = NavigationManager()
    
    var revealVC: SWRevealViewController!
    
    func setFrontViewController(_ viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
        revealVC.pushFrontViewController(navigationController, animated: true)
    }
}

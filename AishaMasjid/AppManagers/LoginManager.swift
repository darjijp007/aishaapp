//
//  LoginManager.swift
//  AishaMasjid
//
//  Created by Jaydip Darji  on 9/4/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class LoginManager {
    static let shared = LoginManager()
    
    func setUpLoginViewController(_ viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
        let sideViewController = SideVC.storyboardViewController()
        NavigationManager.shared.revealVC = SWRevealViewController(rearViewController: sideViewController, frontViewController: navigationController)
        appDelegate.window?.rootViewController = NavigationManager.shared.revealVC
    }
}

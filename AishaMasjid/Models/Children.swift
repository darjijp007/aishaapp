//
//  Children.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation

struct Children {
    var name: String?
    var code: String?
    var age: String?
}

//
//  UtilityManager.swift
//  AishaMasjid
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Jaydip Darji . All rights reserved.
//

import Foundation
import UIKit

class UtilityManager {
    
    static let shared = UtilityManager()
    
    func showAddChildView() -> AddChildrenView {
        let addChildrenView = AddChildrenView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        addChildrenView.tag = 11001
        return addChildrenView
    }
}



import UIKit
import SystemConfiguration
import CoreLocation
import Alamofire
import Toast_Swift

class Helper: NSObject
{
    static let shared = Helper()
   
    func validateEmailWithString(_ checkString : NSString) -> Bool
    {
        let laxString = ".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{3}[A-Za-z]*" as String
        let emailTest = NSPredicate(format:"SELF MATCHES %@",laxString) as NSPredicate
        return emailTest.evaluate(with: checkString)
    }
    
    func ShowAlert(str : String , viewcontroller:UIViewController)
    {
        let alertView = UIAlertController(title: "Alert", message: str as String, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        viewcontroller.navigationController?.present(alertView, animated: true, completion: nil)
    }
    
    func ShowAlert(title: String, message : String , viewcontroller : UIViewController, actions: [UIAlertAction])
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alertView.addAction(action)
        }
        
        viewcontroller.present(alertView, animated: true, completion: nil)
    }
    
    func ShowActionSheet(title: String? = nil, message : String? = nil, viewcontroller : UIViewController, actions: [UIAlertAction])
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        for action in actions {
            alertView.addAction(action)
        }
        
        viewcontroller.present(alertView, animated: true, completion: nil)
    }
    
    func showToast(message: String) {
        UIWindow.topViewController()?.view.hideToast()
        UIWindow.topViewController()?.view.makeToast(message)
    }
        
    func getImageHeight(image: UIImage, view: UIView) -> CGFloat {
        
        if image.size.width > view.frame.size.width {
            return ((image.size.height * view.frame.width) / image.size.width)
        } else {
            return image.size.height
        }
    }
    
    func shareUrl(url: String? = nil, message: String? = nil) {
        
        var items = [Any]()
        
        if let shareUrl = url {
            items.append(NSURL(string:shareUrl)!)
        }
        if let shareMessage = message {
            items.append(shareMessage)
        }
        
        let vc = UIWindow.topViewController()
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc?.view
        vc?.present(activityViewController, animated: true, completion: nil)
    }
    

    func checkNullString(str:NSString) -> NSString
    {
        if str .isKind(of:NSNull.self) || str == "<null>" || str == "0" || str == "(null)" || str == "" || str == "null"
        {
            return ""
        }
        else
        {
            return str
        }
    }
    
    func isNetworkAvailable() -> Bool
    {
        let rechability = NetworkReachabilityManager()
        
        if (rechability?.isReachable)!
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //  MARK:  NSUserDefault Handlers
   
    func setDefaultObject(dict : [String : AnyObject], key:String)
    {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: dict)
        let userDefaults = UserDefaults.standard
        userDefaults.set(encodedData, forKey: key)
    }
    
    func getDefaultObjectForKey(key : String) -> [String : AnyObject]
    {
        let decoded  = UserDefaults.standard.object(forKey: key) as! Data
        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String : AnyObject]
        return decodedTeams
    }
    
    func generateUniqueStr() -> String
    {
        let date = Date()
        let formater = DateFormatter()
        formater.dateFormat = "dd:MM:yyyy:mm:ss:SSS"
        let DateStr = formater.string(from: date) as String
        return DateStr.replacingOccurrences(of: ":", with: "")
    }
    
    func hideFooterSpace(tableView : UITableView)
    {
        tableView.tableFooterView = UIView.init(frame: .zero)
    }
    
    func noDataAvailabe(tableView: UITableView, titleText: String, hide: Bool) {
        if hide == true {
            tableView.tableFooterView = nil
        }
        else {
            let noDataView = NoDataFooterView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: CGFloat(proportionalHeight(150))))
            if titleText != "" {
                noDataView.titleLabel.text = titleText
            }
            tableView.tableFooterView = noDataView
            
        }
    }
    func makeURL(appendURL : NSString , dict : NSMutableDictionary) -> String
    {
        var str = NSString(format:"%@?",appendURL)
        let key = dict.allKeys as NSArray
        let value = dict.allValues as NSArray
        
        for i in 0 ..< key.count
        {
            str = str.appending(NSString(format:"%@=%@&",key[i] as! CVarArg,value[i] as! CVarArg) as String) as NSString
        }
        str = str.substring(to:str.length-1) as NSString
        str = str.replacingOccurrences(of:" ", with: "") as NSString
       // print(str)
        return str as String
    }
    
    func colorFromHex(fromHexString hexString: String?) -> UIColor {
        let rgbValue: UInt = 0
        let scanner = Scanner(string: hexString ?? "")
        scanner.scanLocation = 1
        // bypass '#' character
        return UIColor(red: CGFloat(Double(((Int(rgbValue) & 0xff0000) >> 16)) / 255.0), green: CGFloat(Double(((Int(rgbValue) & 0xff00) >> 8)) / 255.0), blue: CGFloat(Double((Int(rgbValue) & 0xff)) / 255.0), alpha: 1.0)
    }
    
    /*func getError(_ response: ResponseModal) -> NSError {
        return NSError(domain:"", code:500, userInfo:[ NSLocalizedDescriptionKey: response.message!])
    }*/
    
    func getNetworkError() -> NSError {
        return NSError(domain:"", code:errorCode.noNetwork, userInfo:[NSLocalizedDescriptionKey: errorMessages.kNoInternetConnectionMessage])
    }
}
